            public class Course {
                // create a java class named course.java with instance variables/properties for name(string), description(string), seats(int), fee(double), startDate(string), endDate(string), and instructor(user)

                private String name;
                private String description;
                private Integer seats;
                private Double fee;
                private String startDate;
                private String endDate;
                private User instructor;

                // define constructors (both default and parameterized), getters and setters for both classes

                public Course(){
                }

                public Course(String name, String description, Integer seats, Double fee, String startDate, String endDate, User instructor){
                    this.name = name;
                    this.description = description;
                    this.seats = seats;
                    this.fee = fee;
                    this.startDate = startDate;
                    this.endDate = endDate;
                    this.instructor = instructor;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public Integer getSeats() {
                    return seats;
                }

                public void setSeats(Integer seats) {
                    this.seats = seats;
                }

                public Double getFee() {
                    return fee;
                }

                public void setFee(Double fee) {
                    this.fee = fee;
                }

                public String getStartDate() {
                    return startDate;
                }

                public void setStartDate(String startDate) {
                    this.startDate = startDate;
                }

                public String getEndDate() {
                    return endDate;
                }

                public void setEndDate(String endDate) {
                    this.endDate = endDate;
                }

                public User getInstructor() {
                    return this.instructor;
                }

                public void setInstructor(User instructor) {
                    this.instructor = instructor;
                }

                public String getInstructorName(){
                    return this.instructor.getFirstName();
                }


            }
