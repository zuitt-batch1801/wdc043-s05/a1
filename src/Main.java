            public class Main {
                public static void main(String[] args) {

                    /// Instantiate a new User object using the User class' parameterized class constructor

                    // user (firstName, lastName, age, address)
                    User user1 = new User("Mikha", "Ambrosio", 23, "Laguna");

                    // course (name, description, seats, fee, startDate, endDate, instructor)
                    Course course1 = new Course();
                    course1.setName("Calculus");
                    course1.setDescription("Intro to Calculus");
                    course1.setSeats(30);
                    course1.setFee(2500.00);
                    course1.setStartDate("September 1, 2022");
                    course1.setEndDate("December 17, 2022");
                    course1.setInstructor(user1);

                    /// Using the user object's getter methods print the values of its firstName, lastName, age, address
                    System.out.println("User's first name: "+ user1.getFirstName());
                    System.out.println("User's last name: "+ user1.getLastName());
                    System.out.println("User's age: "+ user1.getAge());
                    System.out.println("User's address: "+ user1.getAddress());

                    /// using the course object's getter method print the values of its name, description, fee, and instructor's first name properties
                    System.out.println("Course's name: "+ course1.getName());
                    System.out.println("Course's description: "+ course1.getDescription());
                    System.out.println("Course's seats available: "+ course1.getSeats());
                    System.out.println("Course's instructor's first name: "+course1.getInstructorName());
                }
            }