            public class User {
                // create a java class named User.java with instance variables.properties for: firstName(string), lastName(string), age(int), and address (string)

                private String firstName;
                private String lastName;
                private Integer age;
                private String address;

                // define constructors (both default and parameterized), getters and setters for both classes

                public User(){
                }

                public User(String firstName, String lastName, Integer age, String address){
                    this.firstName = firstName;
                    this.lastName = lastName;
                    this.age = age;
                    this.address = address;
                }

                public String getFirstName() {
                    return firstName;
                }

                public void setFirstName(String firstName) {
                    this.firstName = firstName;
                }

                public String getLastName() {
                    return lastName;
                }

                public void setLastName(String lastName) {
                    this.lastName = lastName;
                }

                public Integer getAge() {
                    return age;
                }

                public void setAge(Integer age) {
                    this.age = age;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }
            }
